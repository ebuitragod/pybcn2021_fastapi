-> pip install fastapi
-> pip install "uvicorn[standard]"
-> pip install typing

1. Create a file main.py
    v0.1. uvicorn main:app --reload in terminal
    v0.2.  http://127.0.0.1:8000/items/5?q=somequery open bro
    
    v1.1. install pydantic 

2. Go to interactive docs:  http://127.0.0.1:8000/docs

3. Alternative documentario that reflects new query parameter and body: http://127.0.0.1:8000/redoc


====
Important:
1. Declare only once the types of parameter, body as function parameters

Documentation:
https://fastapi.tiangolo.com/

